﻿//very good code Yossef 

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform TargetTrans;
    private Light theLight;

	void Start ()
    {
        theLight = GetComponent<Light>();	
	}
	
	void Update ()
    {
        if (TargetTrans != null && theLight != null)
        {
            theLight.transform.position = Vector3.Lerp(transform.position, TargetTrans.position, 0.3f);

            theLight.enabled = TargetTrans.gameObject.activeInHierarchy;
        }		
	}
}
