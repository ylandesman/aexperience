﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileInputHandler : MonoBehaviour
{
    //public float CurrentTotalEnergy = 0.0f;

    public int NumActiveMobiles = 0;

    public AudioSource CoreSound; 

    public static int MaxPlayers = 10;

    public const int NumMessageSections = 8;
    public Quaternion FirstMobileInitRotation = Quaternion.identity;
    [System.Serializable]
    public class MobileDeviceData 
    {
        public string MachineID;
        public float QuatX;
        public float QuatY;
        public float QuatZ;
        public float QuatW;
        public float TimeAlive;
        public bool IsPressedDown;
        public float TimeSinceLastMessage;
        //meta,8992b2800b04fb92b6e53d3d12ce0d2d,360.0f, 360.0f, 360.0f, 3600000,True,16.0f
    }


    public string[] LatestMessage;
    public List<MobileDeviceData> MobileDeviceDatas;

    //public SynthRenderer[] TestSynth;

    void Start ()
    {
        LatestMessage = new string[NumMessageSections];
        MobileDeviceDatas = new List<MobileDeviceData>(10);
        //TestSynth = new SynthRenderer[4];
    }

    int FindMobileDeviceData(string strMachineID)
    {
        for(int i=0; i< MobileDeviceDatas.Count; i++)
        {
            if (MobileDeviceDatas[i].MachineID.Equals(strMachineID))
                return i;
        }

        return -1;
    }

    public void UpdateExistingMachine(int indexMachineID, string[] newMessage)
    {
        if (indexMachineID >= 0 && indexMachineID < MobileDeviceDatas.Count)
        {
            MobileDeviceData data = MobileDeviceDatas[indexMachineID];
            data.MachineID = newMessage[1];
            data.QuatX = float.Parse(newMessage[2]);
            data.QuatY = float.Parse(newMessage[3]);
            data.QuatZ = float.Parse(newMessage[4]);
            data.QuatW = float.Parse(newMessage[5]);
            data.TimeAlive = float.Parse(newMessage[6]);
            data.IsPressedDown = newMessage[7].StartsWith("True");
            data.TimeSinceLastMessage = 0.0f;
            //MobileDeviceDatas.RemoveAt(indexMachineID);
            //MobileDeviceDatas.Add(data);
        }
    }


    public void ProcessNewMessage(string[] newMessage)
    {
        if (newMessage.Length <= 0)
            return;
        else if (newMessage.Length != NumMessageSections && !newMessage[0].Equals("meta"))
            return;

        for(int i=0; i< NumMessageSections; i++)
            LatestMessage[i] = newMessage[i];

        int existingMachineID = FindMobileDeviceData(newMessage[1]);
        if (existingMachineID >= 0)
        {
            // update existing machine 
            UpdateExistingMachine(existingMachineID, newMessage);            
        }
        else
        {
            MobileDeviceData newData = new MobileDeviceData();
            newData.MachineID = newMessage[1];
            newData.QuatX = float.Parse(newMessage[2]);
            newData.QuatY = float.Parse(newMessage[3]);
            newData.QuatZ = float.Parse(newMessage[4]);
            newData.QuatW = float.Parse(newMessage[5]);
            newData.TimeAlive = float.Parse(newMessage[6]);
            newData.IsPressedDown = newMessage[7].StartsWith("True");
            newData.TimeSinceLastMessage = 0.0f;

            MobileDeviceDatas.Add(newData);
        }
    }

    public void UpdateRaniSounds()
    {
        int numActivePlayers = GetNumActiveMobiles();
        //if (CoreSound != null)
        //    CoreSound.volume = Mathf.Lerp(CoreSound.volume, numActivePlayers > 0 ? Mathf.Clamp01(CurrentTotalEnergy / 2.0f) : 0.0f, 0.05f);

        //if (IsMobileDeviceActive(0))
        //    PlayersSoundsA[0].volume = Mathf.Lerp(0.0f, 1.0f, 3.0f * (MobileDeviceDatas[0].Energy));
        //if (IsMobileDeviceActive(1))
        //    PlayersSoundsB[0].volume = Mathf.Lerp(0.0f, 1.0f, 3.0f * (MobileDeviceDatas[1].Energy));
        //if (IsMobileDeviceActive(2))
        //    PlayersSoundsC[0].volume = Mathf.Lerp(0.0f, 1.0f, 3.0f * (MobileDeviceDatas[2].Energy));
        //if (IsMobileDeviceActive(3))
        //    PlayersSoundsD[0].volume = Mathf.Lerp(0.0f, 1.0f, 3.0f * (MobileDeviceDatas[3].Energy));

        //    {
        //        //PlayersSoundsA[0].volume = Mathf.Lerp(PlayersSoundsA[0].volume, 0.0f, 0.1f);
        //        //PlayersSoundsA[0].volume = Mathf.Lerp(PlayersSoundsA[0].volume, 0.0f, 0.1f);
        //        //PlayersSoundsA[1].volume = Mathf.Lerp(PlayersSoundsA[0].volume, 0.0f, 0.1f);
        //        //PlayersSoundsA[2].volume = Mathf.Lerp(PlayersSoundsA[0].volume, 0.0f, 0.1f);
        //    }

        //}
    }




    bool IsMobileDeviceActive(int index)
    {
        if (index >= 0 && index < MobileDeviceDatas.Count)
            return (MobileDeviceDatas[index].TimeSinceLastMessage < 0.5f);

        return false;
    }

    int GetNumActiveMobiles()
    {
        int ret = 0;
        for (int i = 0; i < MobileDeviceDatas.Count; i++)
        {
            if (IsMobileDeviceActive(i))
                ret++;
        }

        return ret;
    }    

    void Update ()
    {
        NumActiveMobiles = GetNumActiveMobiles();

        //CurrentTotalEnergy = 0.0f; //MobileDeviceDatas[i].CurrentEnergyState;
        for (int i = 0; i < MobileDeviceDatas.Count; i++)
        {
            //MobileDeviceData data = MobileDeviceDatas[i];
            //data.TimeSinceLastMessage += Time.deltaTime;
            MobileDeviceDatas[i].TimeSinceLastMessage += Time.deltaTime;

            //if (IsMobileDeviceActive(i))
            //    CurrentTotalEnergy += MobileDeviceDatas[i].Energy;
        }
        UnityEngine.Profiling.Profiler.EndSample();

        //if (CurrentAverageTotalSpin > 0.05f)
        //    CurrentAverageTotalSpin = Mathf.Clamp(CurrentAverageTotalSpin, 0.05f, 0.5f);
        //else
        //    CurrentAverageTotalSpin = 0.0f;
        //float maxFilterValue = 1.0f;
        //float FilterLevel = Mathf.Clamp01(maxFilterValue - maxFilterValue * CurrentAverageTotalSpin * 2.0f);
        //AudioDebugPanel.SetHighPassLevel(FilterLevel);
        //AudioDebugPanel.SetLowPassLevel(FilterLevel);
    }
}
    