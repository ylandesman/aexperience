﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleAttraction : MonoBehaviour
{
    public Transform[] m_Attractors;
    public Transform m_CamTransform;
    public ParticleSystem[] m_CursorParticles;

    ParticleSystem m_System;
    ParticleSystem.Particle[] m_Particles;
    public float m_FarPower = 1.0f;
    //public float m_ClosePower = 10.0f;
    public float m_ActivePower1 = 2.0f;
    public float m_ActivePower2 = -3.0f;
    public float m_NoiseMaxScale = 0.1f;

    public float m_MaxMagnitudeToAttract = 10.0f;
    public float m_MinMagnitudeToAttract = 0.0001f;

    public float[] m_HandOpennessEstimate = { 0.0f, 0.0f };

    public float m_CursorParticlesSize1 = 1.0f;
    public float m_CursorParticlesSize2 = 0.1f;

    public float m_AttractorsForwardOffsetScale = 0.5f;
    public float m_HandOpennessScale = 0.065f;

    public MobileInputHandler theMobileHandler;

    public ParticleSystem m_PhoneParticles;

    public float m_CurrentNoiseAmount = 0.0f;

    private void CalcHandOpennessEstimates()
    {
        m_HandOpennessEstimate[0] = Vector3.Distance(m_Attractors[0].position, m_Attractors[1].position);
        m_HandOpennessEstimate[1] = Vector3.Distance(m_Attractors[2].position, m_Attractors[3].position);
    }

    private void LateUpdate()
    {
        InitializeIfNeeded();

        CalcHandOpennessEstimates();

        // Get particle array
        int numParticlesAlive = m_System.GetParticles(m_Particles);        

        bool mobilePhoneScreenPressed = theMobileHandler != null && theMobileHandler.MobileDeviceDatas.Count > 0 && theMobileHandler.MobileDeviceDatas[0].IsPressedDown;
        if (m_PhoneParticles != null && theMobileHandler != null && theMobileHandler.MobileDeviceDatas != null && theMobileHandler.MobileDeviceDatas.Count > 0)
        {            
            m_PhoneParticles.transform.rotation = Quaternion.Inverse(theMobileHandler.FirstMobileInitRotation) * new Quaternion(-theMobileHandler.MobileDeviceDatas[0].QuatX, -theMobileHandler.MobileDeviceDatas[0].QuatZ, -theMobileHandler.MobileDeviceDatas[0].QuatY, theMobileHandler.MobileDeviceDatas[0].QuatW);
            ParticleSystem.EmissionModule emit = m_PhoneParticles.emission;
            emit.enabled = mobilePhoneScreenPressed;
        }

        if (m_Attractors != null && m_Attractors.Length > 0)
        {
            // count num active attractors 
            int numActiveAttractors = 0;
            for (int a = 0; a < m_Attractors.Length; a++)
            {
                if (m_Attractors[a].gameObject.activeInHierarchy)
                    numActiveAttractors++;
            }

            for (int p = 0; p < m_Particles.Length; p++)
            {
                m_Particles[p].remainingLifetime = 1.0f;
                Vector3 pastVelocity = m_Particles[p].velocity;
                Vector3 newVelocity = Vector3.zero;
                m_Particles[p].velocity = Vector3.zero;

                Vector3 targetDirection;
                for (int a = 0; a < m_Attractors.Length; a++)
                {
                    if (!m_Attractors[a].gameObject.activeInHierarchy)
                        continue;

                    targetDirection = m_Attractors[a].position + m_AttractorsForwardOffsetScale * (m_Attractors[a].position - m_CamTransform.transform.position) - m_Particles[p].position;
                    float power = m_FarPower;
                    if (targetDirection.sqrMagnitude < m_MinMagnitudeToAttract)
                    {
                        power = 0.5f * Mathf.Cos(Time.timeSinceLevelLoad) * Mathf.Sin(Time.timeSinceLevelLoad);
                        newVelocity += (1.0f / (float)numActiveAttractors) * targetDirection * power;
                    }
                    else if (targetDirection.sqrMagnitude < m_MaxMagnitudeToAttract)
                    { 
                        float handOpennessEstimate = a <= 1 ? m_HandOpennessEstimate[0] : m_HandOpennessEstimate[1];
                        float handOpennessValue = Mathf.Clamp01((handOpennessEstimate - m_HandOpennessScale) / m_HandOpennessScale);
                        //handOpennessValue = Mathf.Pow(handOpennessValue, 0.1f);
                        float closePowerBasedOnHandOpenness = Mathf.Lerp(m_ActivePower1, m_ActivePower2, handOpennessValue);
                        power = Mathf.Lerp(closePowerBasedOnHandOpenness, m_FarPower, (targetDirection.sqrMagnitude - m_MinMagnitudeToAttract) / (m_MaxMagnitudeToAttract - m_MinMagnitudeToAttract));

                        if (m_CamTransform != null && power <= 0.0f) // particles pushed away   
                        {
                            targetDirection = Vector3.Lerp(-m_CamTransform.transform.forward, targetDirection, 0.2f);// Vector3.Lerp( m_CamTransform.transform.forward, targetDirection, power/ Mathf.Max(m_ActivePower1, m_ActivePower2 ));
                        }
                        newVelocity += (1.0f / (float)numActiveAttractors) * targetDirection * power;
                        
                        ParticleSystem.NoiseModule noise = m_System.noise;
                        float m_CurrentNoiseAmount = 0.0f;
                        if (mobilePhoneScreenPressed)
                            m_CurrentNoiseAmount = Mathf.Lerp(m_CurrentNoiseAmount, 0.001f, 0.1f);
                        else
                            m_CurrentNoiseAmount = 1.0f + handOpennessValue;
                        noise.strengthX = new ParticleSystem.MinMaxCurve(-m_NoiseMaxScale * m_CurrentNoiseAmount, m_NoiseMaxScale * m_CurrentNoiseAmount);
                        noise.strengthY = new ParticleSystem.MinMaxCurve(-m_NoiseMaxScale * m_CurrentNoiseAmount, m_NoiseMaxScale * m_CurrentNoiseAmount);
                        noise.strengthZ = new ParticleSystem.MinMaxCurve(-m_NoiseMaxScale * m_CurrentNoiseAmount, m_NoiseMaxScale * m_CurrentNoiseAmount);

                        if (a % 2 == 0) // TopFeature
                        {
                            //ParticleSystem.MainModule main = m_CursorParticles[a / 2].main;
                            //main.startSize = (handOpennessValue + 0.1f) * 0.5f;
                            ParticleSystem.SizeOverLifetimeModule size  = m_CursorParticles[a / 2].sizeOverLifetime;
                            size.enabled = true;
                            size.sizeMultiplier = Mathf.Lerp(m_CursorParticlesSize1, m_CursorParticlesSize2, handOpennessValue);

                            ParticleSystem.ShapeModule shape = m_CursorParticles[a / 2].shape;
                            shape.enabled = true;
                            shape.radius = Mathf.Lerp(0.0f, 0.1f, handOpennessValue);


                            m_CursorParticles[a / 2].transform.position = Vector3.Lerp(m_CursorParticles[a / 2].transform.position, m_Attractors[a].position + 0.03f * Vector3.up, 0.3f);
                        }
                    }
                    else
                    {
                        power = m_FarPower;
                        newVelocity += (1.0f / (float)numActiveAttractors) * targetDirection * power;
                    }
                }

                m_Particles[p].velocity = Vector3.Lerp(pastVelocity, newVelocity, 0.1f);
            }

            // Apply the particle changes to the Particle System
            m_System.SetParticles(m_Particles, numParticlesAlive);
        }
    }

    void InitializeIfNeeded()
    {
        if (m_System == null)
            m_System = GetComponent<ParticleSystem>();

        if (m_Particles == null || m_Particles.Length < m_System.main.maxParticles)
            m_Particles = new ParticleSystem.Particle[m_System.main.maxParticles];
    }
}
