﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Net.Sockets;

//[System.Serializable]
//public class PlayerInfo
//{
//    public Vector3 coordinates;
//    //public int lives;
//    //public float health;

//    public static PlayerInfo CreateFromJSON(string jsonString)
//    {
//        return JsonUtility.FromJson<PlayerInfo>(jsonString);
//    }

//    // Given JSON input:
//    // {"name":"Dr Charles","lives":3,"health":0.8}
//    // this example will return a PlayerInfo object with
//    // name == "Dr Charles", lives == 3, and health == 0.8f.
//}

public class UDPNetworkHandler : MonoBehaviour
{
    public MobileInputHandler theMobileInputHandler;
    public static UDPNetworkHandler Instance { get; private set; }
 
    private byte[] Message1Bytes;
    private byte[] Message2Bytes;

    public UdpClient MainUDPClient;
    public UdpClient UDPReceive;
    public bool InputThreadRequired = true;
    //public bool OutputThreadRequired = true;
    public string LastReceivedUDPPacket = "";
    public IPEndPoint TargetToSendTo1;
    public IPEndPoint TargetToSendTo2;

    public int OutputMessageCounter = 0;

//    public bool SendToAllAddresses = false;
//    public bool SendToAddress1 = false;
//    public bool SendToAddress2 = false;


    private int Port1 = 7890;
    private int Port2 = 7891;//7891;
    //private string LocalIP = "127.0.0.1";//"127.0.0.1";
    //public HttpListener HTTPListener;
    private string HostIP1 = "192.168.0.103";
    private string HostIP2 = "192.168.0.105";

    private Thread UDPThreadInput;
    private Thread UDPThreadOutput;
    private bool HandleUDPRequired = true;
    private float ThreadRuntime = 0.0f;

    private bool WritingMessage1 = false;
    private bool WritingMessage2 = false;

    private float m_NetworkThreadSmoothDeltaTime;
    private float m_NetworkThreadDeltaVelocity;
    public float NetworkThreadFPS { get; private set; }
    private long FrameCount = 0;
    //private float NetworkThreadMaxFPS = 120.0f;
    private float NetworkThreadMaxFPS = 60.0f;//110.0f;

    void Start ()
    {
        // set-up LAN data 
        //if (OutputThreadRequired)
        //    MainUDPClient = new UdpClient(Port1); // `new UdpClient()` to auto-pick port
        if (InputThreadRequired)
            UDPReceive = new UdpClient(Port2); // `new UdpClient()` to auto-pick port

        //MessageByteLength = 4 + BytesPerOutgoingMessage;
        //Message1Bytes = new byte[MessageByteLength];
        //Message2Bytes = new byte[MessageByteLength];

        //TargetToSendTo1 = new IPEndPoint(IPAddress.Parse(HostIP1), Port1);
        //TargetToSendTo2 = new IPEndPoint(IPAddress.Parse(HostIP2), Port1);

        // set-up LAN thread 
        if (HandleUDPRequired)
        {
            if (InputThreadRequired)
            {
                UDPThreadInput = new Thread(new ThreadStart(HandleUDPInput));
                UDPThreadInput.IsBackground = true;
                UDPThreadInput.Start();
            }

            //if (OutputThreadRequired)
            //{
            //    UDPThreadOutput = new Thread(new ThreadStart(HandleUDPOutput));
            //    UDPThreadOutput.IsBackground = true;
            //    UDPThreadOutput.Start();
            //}
        }
    }


    void Awake()
    {
        if (Instance != null)
        {
            Debug.LogWarning("multiple instances of UDPNetworkHandler - this instance will be destroyed");
            Destroy(gameObject);
            return;
        }

        Instance = this;

        //DontDestroyOnLoad(gameObject);
    }

    private void OnDestroy()
    {
        if (Instance == this)
        {
            Instance = null;
        }

        HandleUDPRequired = false;

        if (UDPThreadOutput != null)
            UDPThreadOutput.Abort();

        if (UDPThreadInput != null)
            UDPThreadInput.Abort();

        if (MainUDPClient != null)
            MainUDPClient.Close();

        if (UDPReceive != null)
            UDPReceive.Close();
    }

    void UpdateMessagesHeaders()
    {
    }

    void CalcThreadFPS()
    {
        ThreadRuntime = 0.0f;
        DateTime timePrev = DateTime.Now;

        ThreadRuntime += (float)(DateTime.Now - timePrev).TotalSeconds;
        float secondsElapsed = (float)(DateTime.Now - timePrev).TotalSeconds;
        float initialNetworkThreadFPS = 1.0f / (float)secondsElapsed;
        float targetSecondsSpent = (1.0f / NetworkThreadMaxFPS);
        if (initialNetworkThreadFPS > NetworkThreadMaxFPS)
        {
            float timeToSleep = targetSecondsSpent - secondsElapsed;
            //yield return new WaitForSeconds(timeToSleep);
            Thread.Sleep(Mathf.RoundToInt(1000.0f * timeToSleep));
        }
        else if (initialNetworkThreadFPS < 5.0f)
            Thread.Sleep(100);

        secondsElapsed = (float)(DateTime.Now - timePrev).TotalSeconds;
        float actualNetworkThreadFPS = 1.0f / (float)secondsElapsed;
        NetworkThreadFPS = actualNetworkThreadFPS;
        timePrev = DateTime.Now;
        FrameCount++;
    }

    private void FixedUpdate()
    {
        //if (PlayOneShotGood)
        //{
        //    GetComponent<AudioSource>().PlayOneShot(AudioClipGood, 0.4f);
        //    PlayOneShotGood = false;
        //}
    }

    void OnDisable()
    {
        if (UDPThreadInput != null)
            UDPThreadInput.Abort();

        if (UDPThreadOutput != null)
            UDPThreadOutput.Abort();

        if (MainUDPClient != null)
            MainUDPClient.Close();

        if (UDPReceive != null)
            UDPReceive.Close();
    }


    public class ExtendedBehavior : MonoBehaviour
    {
        public void Wait(float seconds, Action action)
        {
            StartCoroutine(_wait(seconds, action));
        }

        IEnumerator _wait(float time, Action callback)
        {
            yield return new WaitForSeconds(time);
            callback();
        }
    }

    //HttpListenerContext context;
    //HttpListenerResponse response;

    
    public int InputMessageCounter = 0;
    //public bool InputLoopEnabled = false;
    //public float[] HACKATOY_TEST_COORDS1 = { 0.0f, 0.0f };
    //public float[] HACKATOY_TEST_COORDS2 = { 0.0f, 0.0f };
    //public float Min_Throw_Distance = 1300.0f;
    //public float[] LAST_THROW_COORDS = { 0.0f, 0.0f };
    //public float HACKATOY_TEST_DISTANCE = 0.0f;
    //public float HACKATOY_TEST_DISTANCE_TOTAL = 0.0f;
    //public float HACKATOY_TEST_INACTIVITY_TIMER = 0.0f;
    //public float NumActiveTracks = 0.0f;
    ////public AudioClip AudioClipBad;
    //public AudioClip AudioClipGood; 

    //public void UpdateActiveTracksForDemo()
    //{
    //    Music.ActiveLoop.Track(MusicSequencer.Track.Pulse1).SetTrackEnabled(true);
    //    Music.ActiveLoop.Track(MusicSequencer.Track.Bass).SetTrackEnabled(false);
    //    Music.ActiveLoop.Track(MusicSequencer.Track.Melody1).SetTrackEnabled(false);
    //    Music.ActiveLoop.Track(MusicSequencer.Track.Snare).SetTrackEnabled(false);
    //    Music.ActiveLoop.Track(MusicSequencer.Track.Chord).SetTrackEnabled(false);
    //    Music.ActiveLoop.Track(MusicSequencer.Track.Melody2).SetTrackEnabled(false);

    //    if (NumActiveTracks >= 1.0f)
    //    {    
    //        Music.ActiveLoop.Track(MusicSequencer.Track.Bass).SetTrackEnabled(true);

    //        if (NumActiveTracks >= 2.0f)
    //            Music.ActiveLoop.Track(MusicSequencer.Track.Kick).SetTrackEnabled(true);
    //        if (NumActiveTracks >= 3.0f)
    //            Music.ActiveLoop.Track(MusicSequencer.Track.Melody1).SetTrackEnabled(true);
    //        if (NumActiveTracks >= 4.0f)
    //            Music.ActiveLoop.Track(MusicSequencer.Track.Snare).SetTrackEnabled(true);
    //        if (NumActiveTracks >= 5.0f)
    //            Music.ActiveLoop.Track(MusicSequencer.Track.Chord).SetTrackEnabled(true);
    //        if (NumActiveTracks >= 6.0f)
    //            Music.ActiveLoop.Track(MusicSequencer.Track.Melody2).SetTrackEnabled(true);
    //    }
    //}

    //public bool PlayOneShotGood = false;
    //public bool PlayOneShotBad = false;

    void HandleUDPInput()
    {
        //HTTPListener.Prefixes.Add("http://localhost:3000/");
        //Utils.LogWarning("Listening..");
        //HTTPListener.Start();

        //int[] ExperienceSetControlValues = new int[Enum.GetNames(typeof(InstallationShowManager.ExperienceSetControlValueType)).Length];
        //for (int i = 0; i < ExperienceSetControlValues.Length; i++)
        //{
        //    // init all control values as true to start with (for now) 
        //    ExperienceSetControlValues[i] = 1;
        //}
        ////Debug.Assert(ExperienceSetControlValues.Length == Enum.GetNames(typeof(InstallationShowManager.ExperienceSetControlValueType)).Length, "--- ExperienceSetControlValues amount does not match enum! Network Failure!! --- ");

        DateTime inputTimePrev = DateTime.Now;
        while (HandleUDPRequired)
        {
            if (UDPReceive != null)
            { 
                string inputText = "";
                try
                {
                    //if (InstallationShowManager.Instance != null)
                    {
                        //Utils.LogWarning("Waiting to receive message...");
                        IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
                        byte[] inputData = UDPReceive.Receive(ref remoteIP);
                        InputMessageCounter++;
                        //Utils.LogWarning("Waiting to receive message2...");
                        inputText = Encoding.UTF8.GetString(inputData);
                        //Utils.LogError("ExternalState: " + inputText);

                        string[] newStateSplitStr = inputText.Split(',');                        
                        //if (newStateSplitStr.Length == 3)
                        //{
                        //    //// 
                        //}
                        //else
                        if(newStateSplitStr[0].Equals("meta"))
                        {
                            if (theMobileInputHandler != null)
                                theMobileInputHandler.ProcessNewMessage(newStateSplitStr);
                            if (InputMessageCounter <= 1)
                                theMobileInputHandler.FirstMobileInitRotation = new Quaternion(-theMobileInputHandler.MobileDeviceDatas[0].QuatX, -theMobileInputHandler.MobileDeviceDatas[0].QuatZ, -theMobileInputHandler.MobileDeviceDatas[0].QuatY, theMobileInputHandler.MobileDeviceDatas[0].QuatW);
                        }

                        LastReceivedUDPPacket = inputText;
                    }
                }
                catch (Exception err)
                {
                    if (HandleUDPRequired && UDPThreadInput.IsAlive)
                    {
                        Debug.LogError("Error with UDP Receive1: " + err.Message + " --------- " + inputText);
                        Debug.LogError("TEST: " + inputText);
                    }
                }
            }
        }

        //InputLoopEnabled = false;
    }

    void HandleUDPOutput()
    {
    //    while (HandleUDPRequired)
    //    {
    //        if (MainUDPClient != null && Message1Bytes != null)
    //        {
    //            try
    //            {
    //                UpdateMessagesHeaders();
    //                UpdateNetworkMessagesWithSurfacePixels();
    //                UpdateNetworkMessagesWithHexPanelPixels();


    //                if (SendToAllAddresses)
    //                {
    //                    if (!WritingMessage1 && !WritingMessage2)
    //                    {
    //                        MainUDPClient.Send(Message1Bytes, Message1Bytes.Length, TargetToSendTo1);
    //                        MainUDPClient.Send(Message2Bytes, Message2Bytes.Length, TargetToSendTo2);
    //                        CalcThreadFPS();
    //                        OutputMessageCounter++;
    //                    }
    //                }
    //                else
    //                {
    //                    if (SendToAddress1)
    //                    {
    //                        if (!WritingMessage1)
    //                            MainUDPClient.Send(Message1Bytes, Message1Bytes.Length, TargetToSendTo1);
    //                        CalcThreadFPS();
    //                        OutputMessageCounter++;
    //                    }
    //                    if (SendToAddress2)
    //                    {
    //                        if (!WritingMessage2)
    //                            MainUDPClient.Send(Message2Bytes, Message2Bytes.Length, TargetToSendTo2);
    //                        CalcThreadFPS();
    //                        OutputMessageCounter++;
    //                    }
    //                }
    //            }
    //            catch (Exception err)
    //            {
    //                if (OutputThreadRequired && HandleUDPRequired)
    //                    Debug.LogError("Could not send UDP message: " + err.Message);
    //            }
    //        }
    //        else
    //        {
    //            Debug.LogError("Skipped UDP messages!");
    //            //    MainUDPClient = new UdpClient(Port1); // `new UdpClient()` to auto-pick port
    //        }
    //    }
    }


    public void OnApplicationQuit()
    {
        HandleUDPRequired = false;

        if (UDPThreadInput != null)
            UDPThreadInput.Abort();

        if (UDPThreadOutput != null)
            UDPThreadOutput.Abort();

        if (MainUDPClient != null)
            MainUDPClient.Close();
    }

    //void OnUdpData(IAsyncResult result)
    //{
    //    // this is what had been passed into BeginReceive as the second parameter:
    //    UdpClient socket = result.AsyncState as UdpClient;
    //    // points towards whoever had sent the message:
    //    IPEndPoint source = new IPEndPoint(0, 0);
    //    // get the actual message and fill out the source:
    //    byte[] message = socket.EndReceive(result, ref source);
    //    // do what you'd like with `message` here:
    //    Console.WriteLine("Got " + message.Length + " bytes from " + source);
    //    // schedule the next receive operation once reading is done:
    //    socket.BeginReceive(new AsyncCallback(OnUdpData), socket);
    //}

 


    //void Update ()
    //{
    //    WritingMessage1 = true;
    //    WritingMessage2 = true;

    //    ////if (SendToAllAddresses || SendToAddress1 || SendToAddress2 || SendToAddress3 || SendToAddress4)
    //    //{
    //    //    UpdateMessagesHeaders();
    //    //    //UpdateNetworkMessagesWithDownloadArrows();
    //    //    UpdateNetworkMessagesWithSurfacePixels();
    //    //}

    //    WritingMessage1 = false;
    //    WritingMessage2 = false;
    //}
}
